= Brushpack | Libre Graphics Meeting
:lang: en-US

[[page]]
[[masthead]]
https://libregraphicsmeeting.org/lgm/[image:vertopal_d9d1ca88d2df4d95a2bdd5ca4456fe1d/f8e12e14aab01af35b57c0a1906f6394d7c332ba.png[image&#44;width=250&#44;height=201]]

== https://libregraphicsmeeting.org/lgm/[Libre Graphics Meeting]

=== annual international convention for free and open source software

==== Menu

link:#content[Skip to content]

* [#menu-item-333]#https://libregraphicsmeeting.org/lgm/[Home]#
* [#menu-item-331]#https://libregraphicsmeeting.org/lgm/about/[About]#
** [#menu-item-332]#https://libregraphicsmeeting.org/lgm/about/organization-team/[Organization
Team]#
** [#menu-item-48]#https://libregraphicsmeeting.org/lgm/about/libre-graphics-meeting/[The
past Libre Graphics Meetings]#
* [#menu-item-334]#https://libregraphicsmeeting.org/lgm/public-documentation/[LGM
set up]#
* [#menu-item-336]#https://libregraphicsmeeting.org/lgm/specs/[Specs]#
* [#menu-item-335]#https://libregraphicsmeeting.org/lgm/ideas/[Ideas]#
* [#menu-item-329]#https://libregraphicsmeeting.org/lgm/?page_id=28[Team]#
** [#menu-item-128]#https://libregraphicsmeeting.org/lgm/?page_id=127[Infrastructure]#
* [#menu-item-465]#https://libregraphicsmeeting.org/lgm/about/libre-graphics-meeting/[Archives]#
* [#menu-item-330]#https://libregraphicsmeeting.org/lgm/contact/[Contact]#

[[main]]
[[primary]]
[[content]]
== Brushpack

_from Boudewijn Rempt on April the 16, 2013 11:15, on the Create
mailling list_

On the last day of LGM 2013, the mypaint, gimp, tupi and krita people
got together, originally to take the first steps towards integrating the
MyPaint brush engine in our applications. This quickly led to a
discussion centered on how to share mypaint brushpacks between
applications, which got generalized into a design session on a bruskpack
file format we can share between apps like krita, gimp and mypaint.

Here’s the proposal :

== Use-cases

* we want a file format that can pack brushes into a single, complete
package that artists can share, install and use directly.
* users can decide whether to download an e.g. mypaint brushpack and
install it for mypaint only, or in a shared location so other
applications that can use that brush type can load it as well. Sharing
is opt-in.

== Installation locations

Following the create spec:

/usr/share/create/brushpacks: global location for shared brushpacks. If
a distro creates a repo package of a brush pack, it should be installed
here.

/usr/share/apps/$APP/…: global location for application-specific
brushpacks. Here the brushpacks bundled with an application are stored

$HOME/$APP_DATA_DIR/…: e.g. .kde/share/apps/krita/

[[:xi]]
brushpacks: here the user installs brushpacks that she doesn’t want to
share with other applications

$HOME/.local/share/create/brushpacks: here the user can install
brushpacks they want to share between application.

On windows, the windows equivalents are used, on OSX ditto.

== Brushpack format

* A zip file that is unzipped on installation.
* The zipfile contains one or more toplevel directories: every toplevel
directory is a brushpack in its own right. The application should use
the directory name as a tag or identifier for the brushpack in the gui.
*  the toplevel contains a manifest.xml file that contains author,
copyright and license information.
* Allowed licenses are :
** proprietary
** public domain
** CC-BY-*

=== Example:

<?xml version=”1.0″ encoding=”UTF-8″?> +
<document-info> +
<about> +
<title>Oils</title> +
<description></description> +
<date>2013-04-06T17:12:10</date> +
<creation-date>2013-02-03T22:01:18</creation-date> +
<url>where you would get a new version</url> +
<license>public domain</license> +
</about> +
<author> +
<full-name>John Doe</full-name> +
<url>http://johndoe.deviantart.com</url> +
<email></email> +
</author> +
</document-info>

* The brushpack directories can contain a tags.xml file that contains
tags for the containing brushes, following the gimp tagging format.
* A brushpack directory contains all the necessary subfolders following
the CREATE spec with the necessary resources to create the brushes. It
is up to the application to decide whether to show brush-specific
resource (patterns, gradients, brush tips) in the generic gui.

=== Example:

deevad.zip for Krita contains

/manifest.xml +
/deevad/ +
/deevad/tags.xml +
/deevad/brushes/gimp (contains .gbr files) +
/deevad/patterns (contains patterns for textured brushes) +
/deevad/gradients/ (contains gradient color sourcess) +
/deevad/krita_paintoppresets (contains krita paintop presets)

For gimp, it would be more like:

/manifest.xml +
/deevad/ +
/deevad/tags.xml +
/deevad/brushes/gimp (contains .gbr files) +
/deevad/gimp_gdyn +
/deevad/gim_gtp

For mypaint +
/manifest.xml +
/deevad/ +
/deevad/tags.xml +
/deevad/mypaint_myb

Note that we didn’t decide on a namespace/naming convention for the kpp,
gtp, myb etc directories. It might also be good to fix brushes/gimp to
follow any new convention (since brushes/gimp can contain abr, gbr, vbr
and gih files, at least).

Brushpacks should not be mixed: so no mypaint brushes and gimp brushes
in one brushpack.

== Mimetype and extension

mimetype: application/x-create-brushpack+zip +
extension: .brushpack

== In this section

* https://libregraphicsmeeting.org/lgm/specs/brushpack/[Brushpack]
* https://libregraphicsmeeting.org/lgm/specs/openraster/[OpenRaster]
* https://libregraphicsmeeting.org/lgm/specs/shared-resources/[Shared
resources]
* https://libregraphicsmeeting.org/lgm/specs/swatches-colour-file-format/[Swatches
Colour File Format]

[[comments]]
[[respond]]
[[reply-title]]
== Leave a Reply [.small]#link:/lgm/specs/brushpack/#respond[Cancel reply]#

[#email-notes]#Your email address will not be published.#
[.required-field-message]#Required fields are marked [.required]#*##

Comment [.required]#*#

Name [.required]#*#

Email [.required]#*#

Website

[[secondary]]
Search for:

== Latest News

* https://libregraphicsmeeting.org/lgm/hello-world/[Launching this
website] [.post-date]#23 April 2013#

== Meta

* https://libregraphicsmeeting.org/lgm/wp/wp-login.php[Log in]
* https://libregraphicsmeeting.org/lgm/feed/[Entries feed]
* https://libregraphicsmeeting.org/lgm/comments/feed/[Comments feed]
* https://wordpress.org/[WordPress.org]

http://wordpress.org/[Proudly powered by WordPress]

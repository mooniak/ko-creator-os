# ko Creator OS

The operating system for the creators of the future. A multi-platform FLOSS (Free/Libre) Creative Suite and GNU/Linux operating system built for the next era of creative freedom and machine-assistead co-creation. Built on elementryOS with Libre.Graphics.

A project by Hanathana Linux Community, hosted by Lankaverse Foundation.